package com.example.testlock.service;

import com.example.testlock.model.Voucher;
import com.example.testlock.repository.VoucherRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Slf4j
public class VoucherService {

    private VoucherRepository voucherRepository;

    @Transactional
    public boolean getOneVoucher(){
        var voucher = this.voucherRepository.findTop1ByIsAvailableTrueOrderByIdAsc();
        if (voucher == null){
            log.info("User failed get voucher");
            return Boolean.FALSE;
        }
        log.info("User get " + voucher.getVoucherCode());
        voucher.setAvailable(Boolean.FALSE);
        voucherRepository.save(voucher);

        return Boolean.TRUE;
    }
}