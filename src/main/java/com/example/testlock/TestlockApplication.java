package com.example.testlock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestlockApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestlockApplication.class, args);
    }

}