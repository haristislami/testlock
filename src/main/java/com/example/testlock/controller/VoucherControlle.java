package com.example.testlock.controller;

import com.example.testlock.model.Voucher;
import com.example.testlock.service.VoucherService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class VoucherControlle {

    private VoucherService voucherService;

    @GetMapping("/voucher")
    public boolean getOneVoucher(){
        return this.voucherService.getOneVoucher();
    }

}